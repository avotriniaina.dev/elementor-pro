# Elementor Pro

## Installation and Activation Guide (Must Read):

- First, install elementor.zip.

- Now install elementor-pro.zip.

- To activate Elementor Pro, Go to your WordPress Dashboard>>Elementor>>License and enter “**GWP7514519919615182316RL**” as a license key (only if it is not already activated).

- If you want to use premade templates or blocks then you have to create a free account on Elementor official website. Elementor will ask you to connect with the Elementor account whenever you try to use premade pages and blocks. Connect and Enjoy [Some newly added pro templates may not work].
Never Update the free version of Elementor from your WordPress Server. It can break the template importing feature.
- If you’re facing any issues first deactivate and delete the Elementor plugin then open phpMyAdmin to find and delete database entries containing “elementor” in them into the wp_options & wp_usermeta tables only. After deleting elementor entries from both tables, reinstall and use the plugins.

## Guide d'installation et d'activation (doit lire):


- Tout d'abord, installez elementor.zip.


- Installez maintenant elementor-pro.zip.


- Pour activer Elementor Pro, accédez à votre tableau de bord WordPress>>Elementor>>Licence et entrez "GWP7514519919615182316RL" comme clé de licence (uniquement si elle n'est pas déjà activée).


- Si vous souhaitez utiliser des modèles ou des blocs prédéfinis, vous devez créer un compte gratuit sur le site officiel d'Elementor. Elementor vous demandera de vous connecter au compte Elementor chaque fois que vous essayez d'utiliser des pages et des blocs prédéfinis. Connectez-vous et profitez [Certains modèles professionnels récemment ajoutés peuvent ne pas fonctionner].
Ne mettez jamais à jour la version gratuite d'Elementor à partir de votre serveur WordPress. Cela peut casser la fonction d'importation de modèles.


- Si vous rencontrez des problèmes, désactivez et supprimez d'abord le plugin Elementor, puis ouvrez phpMyAdmin pour rechercher et supprimer les entrées de base de données contenant "elementor" dans les tables wp_options et wp_usermeta uniquement. Après avoir supprimé les entrées elementor des deux tables, réinstallez et utilisez les plugins.
